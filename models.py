from peewee import *
from config import config

db = MySQLDatabase(**config.database)


class BaseModel(Model):
    class Meta:
        database = db


class Def(BaseModel):
    define = IntegerField()
    start = IntegerField()
    end = IntegerField()
    region = CharField(max_length=40)


def create_tables():
    Def.create_table()


def drop_tables():
    Def.drop_table()


if __name__ == '__main__':
    drop_tables()
    create_tables()
