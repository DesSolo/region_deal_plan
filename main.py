import requests
from os import path
from csv import DictReader
from config import config
from models import Def, db


class WorkerCSV(object):
    def __init__(self, name):
        self.name = name
        self.url = 'https://rossvyaz.ru/docs/articles/'

    def exist_file(self):
        return path.isfile(self.name)

    def download(self):
        response = requests.get(self.url + self.name, stream=True)
        with open(self.name, 'wb') as file:
            for chunk in response:
                file.write(chunk)

    def read(self):
        with open(self.name, 'r', encoding='cp1251') as file:
            for row in DictReader(file, delimiter=';'):
                yield row


def prettify(source):
    payload = {}
    for key in source:
        if key not in config.aliases:
            continue
        payload[config.aliases[key]] = source[key]
    if '|' in payload['region']:
        payload['region'] = payload['region'].split('|')[-1]
    payload['start'] = int(payload['start'])
    payload['end'] = int(payload['end'])
    return payload


for file in config.files:
    csv_file = WorkerCSV(file)
    if not csv_file.exist_file():
        csv_file.download()
    for item in csv_file.read():
        item = prettify(item)
        define, result = Def.get_or_create(**item)
        if result:
            print(define.id, define.region, 'Add success')
        else:
            print(define.id, define.region, 'Exist in database')